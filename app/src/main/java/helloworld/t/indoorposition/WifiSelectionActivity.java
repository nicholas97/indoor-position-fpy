package helloworld.t.indoorposition;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import helloworld.t.indoorposition.Adapter.WifiScanAdapter;
import helloworld.t.indoorposition.Adapter.WifiScanAdapter1;
import helloworld.t.indoorposition.Common.Common;
import helloworld.t.indoorposition.Model.RoomWifi;
import helloworld.t.indoorposition.Model.Wifi;

public class WifiSelectionActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    RelativeLayout rootLayout;

    FloatingActionButton fab,fabScan;

    //Firebase
    FirebaseDatabase db;
    DatabaseReference wifiList;

    private WifiManager wifiManager;

    WifiScanAdapter1 adapter;

    List<ScanResult> scanResults=new ArrayList<>();
    List<Wifi> wifis=new ArrayList<>();
    boolean wifiLoad=false
            ;

    private final int MY_PERMISSIONS_ACCESS_COARSE_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_selection);


        db=FirebaseDatabase.getInstance();
        wifiList=db.getReference("Wifis");

        wifiList.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                wifiLoad=false;
                wifis=new ArrayList<>();
                for (DataSnapshot d: dataSnapshot.getChildren())
                {
                    wifis.add(d.getValue(Wifi.class));



                }
                wifiLoad=true;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //Init
        recyclerView=(RecyclerView)findViewById(R.id.recycler_wifi_selection);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        rootLayout=(RelativeLayout)findViewById(R.id.rootLayoutWifiSelectionList);

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled()) {
            Toast.makeText(getApplicationContext(), "Turning WiFi ON...", Toast.LENGTH_LONG).show();
            wifiManager.setWifiEnabled(true);
        }



        fabScan=(FloatingActionButton)findViewById(R.id.fabScan);
        fabScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(WifiSelectionActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                            WifiSelectionActivity.this,
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_ACCESS_COARSE_LOCATION
                    );
                } else {
                    if(wifiLoad==true) {
                        wifiManager.startScan();
                        scanResults = wifiManager.getScanResults();


                        filterWifi();

                        adapter = new WifiScanAdapter1(getApplicationContext(), scanResults);
                        recyclerView.setAdapter(adapter);

                    }else{
                        Toast.makeText(WifiSelectionActivity.this,"Loding Wifis Detail", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        fab=(FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent updateDistance=new Intent(WifiSelectionActivity.this,UpdateDistanceActivity.class);
                updateDistance.putExtra("roomId",getIntent().getStringExtra("roomId"));
                startActivity(updateDistance);finish();

            }
        });


        if(getIntent()!=null){
            Common.roomId=getIntent().getStringExtra("roomId");
        }



    }
    private void filterWifi(){
        List<ScanResult> scanResults1=new ArrayList<>();
        scanResults1=scanResults;
        boolean matched;
        for(int i=0;i<scanResults1.size();i++){
            matched=false;
            for(int j=0;j<wifis.size();j++){
                if(scanResults.get(i).BSSID.toString().equals(wifis.get(j).getMacAddress())){
                    matched=true;
                    break;
                }
            }
            if(!matched){
                scanResults.remove(i);
                Log.d("Wifi selection","Remove");
            }
        }

    }
}
