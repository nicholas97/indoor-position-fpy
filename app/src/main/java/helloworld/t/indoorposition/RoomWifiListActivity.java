package helloworld.t.indoorposition;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import helloworld.t.indoorposition.Interface.ItemClickListener;
import helloworld.t.indoorposition.Model.Room;
import helloworld.t.indoorposition.Model.RoomWifi;
import helloworld.t.indoorposition.ViewHolder.RoomViewHolder;
import helloworld.t.indoorposition.ViewHolder.RoomWifiViewHolder;

public class RoomWifiListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    RelativeLayout rootLayout;

    FloatingActionButton fab;

    FirebaseDatabase db;
    DatabaseReference roomWifiList;

    String buildingId="",roomId="";

    FirebaseRecyclerAdapter<RoomWifi, RoomWifiViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_wifi_list);


        db=FirebaseDatabase.getInstance();
        roomWifiList=db.getReference("RoomWifis");


        //Init
        recyclerView=(RecyclerView)findViewById(R.id.recycler_room_wifi);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        rootLayout=(RelativeLayout)findViewById(R.id.rootLayoutRoomWifi);

        fab=(FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(RoomWifiListActivity.this, WifiSelectionActivity.class);
                intent.putExtra("roomId",roomId);
                startActivity(intent);
            }
        });
        if(getIntent()!=null){
            roomId=getIntent().getStringExtra("roomId");
        }
            loadListRoomWifi(roomId);


    }


    private void loadListRoomWifi(final String roomId) {

        Query listRoomWifi=roomWifiList.orderByChild("roomId").equalTo(roomId);

        FirebaseRecyclerOptions<RoomWifi> options=new FirebaseRecyclerOptions.Builder<RoomWifi>()
                .setQuery(listRoomWifi,RoomWifi.class)
                .build();
        adapter=new FirebaseRecyclerAdapter<RoomWifi, RoomWifiViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull RoomWifiViewHolder viewHolder, int position, @NonNull RoomWifi model) {
                viewHolder.txtWifiName.setText(model.getWifiName()+" ("+model.getMacAddress()+")");
                viewHolder.txtMaxDistance.setText("Maximum Distance : "+model.getMaxDistance());
                viewHolder.txtMinDistance.setText("Minimum Distance : "+model.getMinDistance());

            }

            @NonNull
            @Override
            public RoomWifiViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View itemView= LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.room_wifi_item,viewGroup,false);
                return new RoomWifiViewHolder(itemView);
            }
        };

        adapter.startListening();
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }
}
