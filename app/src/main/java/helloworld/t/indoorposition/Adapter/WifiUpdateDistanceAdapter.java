package helloworld.t.indoorposition.Adapter;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import helloworld.t.indoorposition.Common.Common;
import helloworld.t.indoorposition.Model.RoomWifi;
import helloworld.t.indoorposition.Model.Strength;
import helloworld.t.indoorposition.Model.Wifi;
import helloworld.t.indoorposition.Model.WifiUtils;
import helloworld.t.indoorposition.R;

public class WifiUpdateDistanceAdapter extends RecyclerView.Adapter<WifiUpdateDistanceAdapter.ViewHolder> {

    private Context context;
    private List<RoomWifi> scanResults;
    private static DecimalFormat df2 = new DecimalFormat("#.##");


    public WifiUpdateDistanceAdapter(Context context, List<RoomWifi> scanResults){
        this.context=context;
        this.scanResults=scanResults;
    }
    @NonNull
    @Override
    public WifiUpdateDistanceAdapter.ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        View view;
        view= LayoutInflater.from(context).inflate(R.layout.room_wifi_item_update_distance,parent,false);

        final WifiUpdateDistanceAdapter.ViewHolder viewHolder=new WifiUpdateDistanceAdapter.ViewHolder(view);



        viewHolder.wifi_Room_Update_Distance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                //Snackbar.make(view,"Waiting Code",Snackbar.LENGTH_SHORT).show();



            }
        });

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull WifiUpdateDistanceAdapter.ViewHolder holder, int position) {
        RoomWifi scanResult =scanResults.get(position);


        holder.txtWifiNameUpdateDistance.setText(scanResult.getWifiName()+"("+scanResult.getMacAddress()+")");
        holder.txtMinDistanceUpdateDistance.setText("Minimum Distance :"+df2.format(Double.valueOf(scanResult.getMinDistance()))+" m");
        holder.txtMaxDistanceUpdateDistance.setText("Maximum Distance :"+df2.format(Double.valueOf(scanResult.getMaxDistance()))+" m");
    }

    @Override
    public int getItemCount() {
        return scanResults.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtWifiNameUpdateDistance,txtMinDistanceUpdateDistance,txtMaxDistanceUpdateDistance;
        ImageView wifiSignalImg;
        ConstraintLayout wifi_Room_Update_Distance;
        public ViewHolder(@NonNull View itemView){
            super(itemView);
            txtWifiNameUpdateDistance=itemView.findViewById(R.id.txtWifiNameUpdateDistance);
            txtMinDistanceUpdateDistance=itemView.findViewById(R.id.txtMinDistanceUpdateDistance);
            txtMaxDistanceUpdateDistance=itemView.findViewById(R.id.txtMaxDistanceUpdateDistance);
            wifiSignalImg=itemView.findViewById(R.id.wifiSignalImg);
            wifi_Room_Update_Distance=(ConstraintLayout) itemView.findViewById(R.id.wifiRoomUpdateDistance);

        }

    }
}