package helloworld.t.indoorposition;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import helloworld.t.indoorposition.Common.Common;
import helloworld.t.indoorposition.Interface.ItemClickListener;
import helloworld.t.indoorposition.Model.Building;
import helloworld.t.indoorposition.Model.Room;
import helloworld.t.indoorposition.Model.Wifi;
import helloworld.t.indoorposition.ViewHolder.BuildingViewHolder;
import helloworld.t.indoorposition.ViewHolder.RoomViewHolder;
import helloworld.t.indoorposition.ViewHolder.WifiViewHolder;

public class WifiListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    RelativeLayout rootLayout;

    FloatingActionButton fab;

    //Firebase
    FirebaseDatabase db;
    DatabaseReference wifiList;
    FirebaseRecyclerAdapter<Wifi, WifiViewHolder> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_list);


        db=FirebaseDatabase.getInstance();
        wifiList=db.getReference("Wifis");

        //Init
        recyclerView=(RecyclerView)findViewById(R.id.recycler_wifi);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        rootLayout=(RelativeLayout)findViewById(R.id.rootLayoutWifiList);

        fab=(FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addWifi=new Intent(WifiListActivity.this,AddWifiActivity.class);
                startActivity(addWifi);

            }
        });

        loadListWifi();
    }

    private void loadListWifi() {

        Query listWifi=wifiList.orderByChild("id");

        FirebaseRecyclerOptions<Wifi> options=new FirebaseRecyclerOptions.Builder<Wifi>()
                .setQuery(listWifi,Wifi.class)
                .build();
        adapter=new FirebaseRecyclerAdapter<Wifi, WifiViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull WifiViewHolder viewHolder, int position, @NonNull Wifi model) {
                viewHolder.txtWifiName.setText(model.getName());
                viewHolder.txtMacAddress.setText(model.getMacAddress());

                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //Coding
                    }
                });
            }

            @NonNull
            @Override
            public WifiViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View itemView= LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.wifi_item,viewGroup,false);
                return new WifiViewHolder(itemView);
            }
        };

        adapter.startListening();
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getTitle().equals(Common.DELETE))
        {

            deleteWifi(adapter.getRef(item.getOrder()).getKey());
        }
        return super.onContextItemSelected(item);
    }
    private void deleteWifi(String key) {

        wifiList.child(key).removeValue();
        Log.d("Delte wifi","dsdwsqedfwedewwwwwwwww");
        Snackbar.make(rootLayout,"Wifi was deleted",Snackbar.LENGTH_SHORT).show();
    }
}
