package helloworld.t.indoorposition.Model;

import java.util.ArrayList;
import java.util.List;

public class RoomDetail {
    private String roomId;
    private Room room;
    private List<RoomWifi> roomWifis =new ArrayList<>();

    public RoomDetail(String roomId, Room room) {
        this.roomId = roomId;
        this.room = room;
    }

    public RoomDetail() {
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public List<RoomWifi> getRoomWifis() {
        return roomWifis;
    }

    public void setRoomWifis(List<RoomWifi> roomWifis) {
        this.roomWifis = roomWifis;
    }
}
