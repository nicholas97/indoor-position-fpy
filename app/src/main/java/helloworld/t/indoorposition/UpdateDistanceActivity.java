package helloworld.t.indoorposition;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import helloworld.t.indoorposition.Adapter.WifiScanAdapter1;
import helloworld.t.indoorposition.Adapter.WifiUpdateDistanceAdapter;
import helloworld.t.indoorposition.Common.Common;
import helloworld.t.indoorposition.Model.RoomWifi;
import helloworld.t.indoorposition.Model.WifiUtils;

public class UpdateDistanceActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    RelativeLayout rootLayout;

    FloatingActionButton fabClear,fabUpdate,fabUpload;

    //Firebase
    FirebaseDatabase db;
    DatabaseReference roomWifiList;
    DatabaseReference roomList;

    private RecyclerView.Adapter mAdapter;

    String roomId="";

    private WifiManager wifiManager;

    List<ScanResult> scanResults=new ArrayList<>();


    private final int MY_PERMISSIONS_ACCESS_COARSE_LOCATION = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_distance);

        db=FirebaseDatabase.getInstance();
        roomWifiList=db.getReference("RoomWifis");
        roomList=db.getReference("Rooms");

        recyclerView=(RecyclerView)findViewById(R.id.recycler_update_distance);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        rootLayout=(RelativeLayout)findViewById(R.id.rootLayoutUpdateDistance);


        fabClear=(FloatingActionButton)findViewById(R.id.fabClear);
        fabClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Common.rws=null;
                loadInfo();
                Snackbar.make(rootLayout,"Clear Completed",Snackbar.LENGTH_SHORT).show();
                finish();

            }
        });

        fabUpdate=(FloatingActionButton)findViewById(R.id.fabUpdate);
        fabUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDistance();
                loadInfo();
                Snackbar.make(rootLayout,"Updated",Snackbar.LENGTH_SHORT).show();

            }
        });

        fabUpload=(FloatingActionButton)findViewById(R.id.fabUpload);
        fabUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roomWifiList.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot d: dataSnapshot.getChildren()){
                            RoomWifi wifi = d.getValue(RoomWifi.class);
                            Log.d("Test",wifi.getWifiName());
                            if(roomId.equals(wifi.getRoomId())){
                                if(Common.rws.size()>0) {
                                    roomWifiList.child(d.getKey()).removeValue();
                                }else{

                                    Snackbar.make(rootLayout,"No Wifi Detail Detected",Snackbar.LENGTH_SHORT).show();
                                }
                            }
                            //Snackbar.make(rootLayout,"Id"+roomId+"/"+wifi.getRoomId(),Snackbar.LENGTH_SHORT).show();

                        }
                        addNewRoomWifi();

                        loadInfo();
                        finish();

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                //Snackbar.make(rootLayout,"Updating",Snackbar.LENGTH_SHORT).show();


            }
        });
        if(getIntent()!=null){
            roomId=getIntent().getStringExtra("roomId");
        }
        loadInfo();

    }

    private void loadInfo() {
        if(Common.rws==null){
            Common.rws = new ArrayList<>();}
        mAdapter = new WifiUpdateDistanceAdapter(this,Common.rws);
        recyclerView.setAdapter(mAdapter);
    }
    private void addNewRoomWifi(){

        for(int i=0;i<Common.rws.size();i++) {
            roomWifiList.push().setValue(Common.rws.get(i));
        }
        Common.rws=null;

    }
    private void updateDistance(){
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled()) {
            Toast.makeText(getApplicationContext(), "Turning WiFi ON...", Toast.LENGTH_LONG).show();
            wifiManager.setWifiEnabled(true);
        }

        if (ActivityCompat.checkSelfPermission(UpdateDistanceActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    UpdateDistanceActivity.this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_ACCESS_COARSE_LOCATION
            );
        } else {
            wifiManager.startScan();
            scanResults = wifiManager.getScanResults();

            for(int i=0;i<scanResults.size();i++){
                for(int j=0;j<Common.rws.size();j++) {
                    if (scanResults.get(i).BSSID.toString().equals(Common.rws.get(j).getMacAddress())) {
                        double distance = WifiUtils.calculateDistance(scanResults.get(i).frequency, scanResults.get(i).level);

                        if(Double.valueOf(Common.rws.get(j).getMinDistance())>distance){
                            Common.rws.get(j).setMinDistance(String.valueOf(distance));
                        }
                        if(Double.valueOf(Common.rws.get(j).getMaxDistance())<distance){
                            Common.rws.get(j).setMaxDistance(String.valueOf(distance));
                        }


                    }
                }

            }


        }
    }


}
