package helloworld.t.indoorposition.ViewHolder;

import android.view.ContextMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import helloworld.t.indoorposition.Common.Common;
import helloworld.t.indoorposition.Interface.ItemClickListener;
import helloworld.t.indoorposition.R;

public class RoomWifiViewHolder extends RecyclerView.ViewHolder{

    public TextView txtWifiName,txtMinDistance,txtMaxDistance;


    public RoomWifiViewHolder(View itemView){
        super(itemView);

        txtWifiName=itemView.findViewById(R.id.txtWifiName);
        txtMinDistance=itemView.findViewById(R.id.txtMinDistance);
        txtMaxDistance=itemView.findViewById(R.id.txtMaxDistance);

    }


}
