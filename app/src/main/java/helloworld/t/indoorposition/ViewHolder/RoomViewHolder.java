package helloworld.t.indoorposition.ViewHolder;

import android.view.ContextMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import helloworld.t.indoorposition.Common.Common;
import helloworld.t.indoorposition.Interface.ItemClickListener;
import helloworld.t.indoorposition.R;

public class RoomViewHolder extends RecyclerView.ViewHolder implements
        View.OnClickListener,
        View.OnCreateContextMenuListener {

    public TextView txtRoomName;
    public ImageView imageView;

    private ItemClickListener itemClickListener;

    public RoomViewHolder(View itemView){
        super(itemView);

        txtRoomName=itemView.findViewById(R.id.room_name);
        imageView=itemView.findViewById(R.id.room_image);

        itemView.setOnCreateContextMenuListener(this);
        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.setHeaderTitle("Select the action");

        contextMenu.add(0,0,getAdapterPosition(), Common.UPDATE);
        contextMenu.add(0,1,getAdapterPosition(),Common.DELETE);
    }
}
