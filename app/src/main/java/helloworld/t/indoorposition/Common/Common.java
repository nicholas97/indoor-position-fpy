package helloworld.t.indoorposition.Common;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import helloworld.t.indoorposition.Model.RoomWifi;

public class Common {

    public static final String UPDATE="Update";
    public static final String DELETE="Delete";

    public static final int PICK_IMAGE_REQUEST=71;

    public static List<RoomWifi> rws;

    public static String roomId;
}
