package helloworld.t.indoorposition.Adapter;


import android.content.Context;
import android.net.wifi.ScanResult;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import helloworld.t.indoorposition.AddWifiActivity;
import helloworld.t.indoorposition.Common.Common;
import helloworld.t.indoorposition.Model.Strength;
import helloworld.t.indoorposition.Model.Wifi;
import helloworld.t.indoorposition.Model.WifiUtils;
import helloworld.t.indoorposition.R;

public class WifiScanAdapter extends RecyclerView.Adapter<WifiScanAdapter.ViewHolder> {

    private Context context;
    private List<ScanResult> scanResults;

    FirebaseDatabase db;
    DatabaseReference wifiList;


    List<Wifi> wifiList1;



    public WifiScanAdapter(Context context, List<ScanResult> scanResults){
        this.context=context;
        this.scanResults=scanResults;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        View view;
        view= LayoutInflater.from(context).inflate(R.layout.wifi_result_item,parent,false);

        final ViewHolder viewHolder=new ViewHolder(view);
        db=FirebaseDatabase.getInstance();
        wifiList=db.getReference("Wifis");

        wifiList.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                wifiList1 = new ArrayList<>();

                for (DataSnapshot d: dataSnapshot.getChildren()){
                    Wifi wifi = d.getValue(Wifi.class);
                    wifiList1.add(wifi);
                }
                Log.d("On data changed","hereeeeeeeeeeeeeeeeeee");

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        viewHolder.wifi_scan_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final Wifi newWifi=new Wifi();
                newWifi.setName(viewHolder.textName.getText().toString());
                newWifi.setMacAddress(viewHolder.textMacAddress.getText().toString());


                addWifi(newWifi,wifiList1,view);

            }
        });

        return viewHolder;
    }
    private void addWifi(Wifi newWifi,List<Wifi> wifiList,View view){
        Log.d("add Wifi","hereeeeeeeeeeeeeeeeeee");

        boolean result=true;

        for(int i=0;i<wifiList.size();i++){
            if(wifiList.get(i).getMacAddress().equals(newWifi.getMacAddress())){
                result=false;
            }

        }
        if(result) {
            addNewWifi(newWifi);
            Snackbar.make(view,"New Wifi "+newWifi.getName()+" added",Snackbar.LENGTH_SHORT).show();

        }else{
            Snackbar.make(view,"Wifi added. Please Try Other",Snackbar.LENGTH_SHORT).show();
        }
    }
    public void addNewWifi(Wifi newWifi){
        wifiList.push().setValue(newWifi);
        wifiList1.add(newWifi);


    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ScanResult scanResult =scanResults.get(position);
        Strength strength=Strength.calculate(scanResult.level);

        holder.textName.setText(scanResult.SSID);
        holder.textMacAddress.setText(scanResult.BSSID);
        holder.textDesc.setText(scanResult.level+"dBm "+scanResult.frequency+"MHz ~"+ WifiUtils.calculateDistance(scanResult.frequency,scanResult.level)+"m");
        holder.wifiSignalImg.setImageResource(strength.imageResource());
        holder.wifiSignalImg.setColorFilter(strength.colorResource());
    }

    @Override
    public int getItemCount() {
        return scanResults.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textName,textMacAddress,textDesc;
        ImageView wifiSignalImg;
        ConstraintLayout wifi_scan_view;
        public ViewHolder(@NonNull View itemView){
            super(itemView);
            textName=itemView.findViewById(R.id.textName);
            textMacAddress=itemView.findViewById(R.id.textMacAddress);
            textDesc=itemView.findViewById(R.id.textDesc);
            wifiSignalImg=itemView.findViewById(R.id.wifiSignalImg);
            wifi_scan_view=(ConstraintLayout) itemView.findViewById(R.id.wifiScanView);

        }

    }
}
