package helloworld.t.indoorposition;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import helloworld.t.indoorposition.Common.Common;
import helloworld.t.indoorposition.Model.Room;
import helloworld.t.indoorposition.Model.RoomDetail;
import helloworld.t.indoorposition.Model.RoomWifi;
import helloworld.t.indoorposition.Model.WifiUtils;

public class ScanActivity extends AppCompatActivity {
    TextView txtLocationName;
    Button btnScan;
    ImageView scan_image;

    FirebaseDatabase db;
    DatabaseReference roomList;
    DatabaseReference wifiRoomList;
    FirebaseStorage storage;
    StorageReference storageReference;


    private WifiManager wifiManager;
    List<ScanResult> scanResults=new ArrayList<>();
    private final int MY_PERMISSIONS_ACCESS_COARSE_LOCATION = 1;

    List<RoomDetail> roomDetails=new ArrayList<>();
    List<RoomWifi> roomWifis=new ArrayList<>();

    RoomDetail result=new RoomDetail();

    boolean roomsLoad,roomWifiLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        db=FirebaseDatabase.getInstance();
        roomList=db.getReference("Rooms");
        wifiRoomList=db.getReference("RoomWifis");
        storage=FirebaseStorage.getInstance();
        storageReference=storage.getReference();

        btnScan=findViewById(R.id.btnScan);
        scan_image=findViewById(R.id.scan_image);
        txtLocationName=findViewById(R.id.txtLocationName);

        roomList.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot d: dataSnapshot.getChildren())
                {
                    RoomDetail rd=new RoomDetail(d.getKey(),(d.getValue(Room.class)));
                    roomDetails.add(rd);



                }
                roomsLoad=true;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        wifiRoomList.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot d: dataSnapshot.getChildren())
                {
                    roomWifis.add(d.getValue(RoomWifi.class));



                }
                roomWifiLoad=true;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startScan();
                startDetect();
                Log.d("Check Item : ",String.valueOf(roomWifis.size()));
                Log.d("Check Item : ",String.valueOf(roomDetails.size()));
                if(result.getRoomId()!=null){
                    txtLocationName.setText(result.getRoom().getName());
                    Picasso.with(getBaseContext()).load(result.getRoom().getPhoto()).into(scan_image);
                    result=new RoomDetail();
                    result.setRoomId(null);
                }else{
                    txtLocationName.setText("No Location Detected");
                    scan_image.setImageDrawable(getResources().getDrawable(R.drawable.scanlocation));
                }
            }
        });



    }
    private void startScan(){
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled()) {
            Toast.makeText(getApplicationContext(), "Turning WiFi ON...", Toast.LENGTH_LONG).show();
            wifiManager.setWifiEnabled(true);
        }

        if (ActivityCompat.checkSelfPermission(ScanActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    ScanActivity.this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_ACCESS_COARSE_LOCATION
            );
        } else {
            wifiManager.startScan();
            scanResults = wifiManager.getScanResults();

        }
    }

    private void startDetect(){

    if(roomWifiLoad &&roomsLoad) {

    for (int i = 0; i < roomDetails.size(); i++) {
        for (int j = 0; j < roomWifis.size(); j++) {
            if (roomDetails.get(i).getRoomId().equals(roomWifis.get(j).getRoomId())) {
                List<RoomWifi> roomWifis1 = new ArrayList<RoomWifi>();
                roomWifis1=roomDetails.get(i).getRoomWifis();

                roomWifis1.add(roomWifis.get(j));
                roomDetails.get(i).setRoomWifis(roomWifis1);
            }
        }
    }


    for (int i = 0; i < roomDetails.size(); i++) {
        RoomDetail rd = roomDetails.get(i);
        int count = 0;
        if( rd.getRoomWifis().size()==0){
            count=-1;

        }
        for (int j = 0; j < rd.getRoomWifis().size(); j++) {

            RoomWifi rw = rd.getRoomWifis().get(j);
            for (int k = 0; k < scanResults.size(); k++) {
                if (rw.getMacAddress().equals(scanResults.get(k).BSSID.toString())) {
                    System.out.println("Matched");
                    boolean result = checkDistance(rw, WifiUtils.calculateDistance(scanResults.get(k).frequency, scanResults.get(k).level));

                    if (result) {
                        count++;
                    }

                }else{System.out.println("Not Matched");}

            }
        }
        if (rd.getRoomWifis().size() == count) {
            result = rd;
        }

    }
}



    }
    private boolean checkDistance(RoomWifi rw,double distance){
        boolean result=false;

        if(Double.valueOf(rw.getMinDistance())<=distance && Double.valueOf(rw.getMaxDistance())>=distance){result=true;}


        return result;
    }
}
