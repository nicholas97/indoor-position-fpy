package helloworld.t.indoorposition.Model;

public class Room {
    private String buildingId,name,photo,numWifi;

    public Room() {
    }

    public Room(String buildingId, String name, String photo, String numWifi) {
        this.buildingId = buildingId;
        this.name = name;
        this.photo = photo;
        this.numWifi = numWifi;
    }

    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getNumWifi() {
        return numWifi;
    }

    public void setNumWifi(String numWifi) {
        this.numWifi = numWifi;
    }
}
